Help me build a full stack chat app similar to discord, slack, and rocketchat. I want to use React, Tailwind, NextJS, Storybook and Golang for the backend. I want it to be highly scalable and running on kubernetes. Also should use any technology that would combine the effort of designing the mobile apps along with the web app to ensure the client side doesn't need to be built 3 times for web/android/ios.


```
npx create-next-app@latest my-chat-app
cd my-chat-app
npm install tailwindcss postcss autoprefixer
npx tailwindcss init -p
```

Storybook
```
npx sb init
```

