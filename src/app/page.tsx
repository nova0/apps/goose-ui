// import React from 'react';
// // Import other components as needed

// const MainPage: React.FC = () => {
//   return (
//     <div>
//       {/* Your main page content */}
//     </div>
//   );
// };

// export default MainPage;


import React from 'react';
import Sidebar from '../components/SideBar';
import ChatWindow from '../components/ChatWindow';
import UserProfile from '../components/UserProfile';

const Home: React.FC = () => {
  return (
    <div className="flex">
      <Sidebar />
      <ChatWindow />
      <UserProfile />
    </div>
  );
};

export default Home;
