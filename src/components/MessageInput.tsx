// MessageInput.tsx
import React from 'react';

const MessageInput: React.FC = () => {
  return (
    <form className="message-input mt-4">
      <input className="border p-2 w-full" type="text" placeholder="Type a message..." />
      <button className="bg-blue-500 text-white p-2 mt-2" type="submit">Send</button>
    </form>
  );
};

export default MessageInput;
