import React from 'react';
import MessageList from './MessageList';
import MessageInput from './MessageInput';

const ChatWindow: React.FC = () => {
  return (
    <div className="flex-grow bg-gray-100">
      <MessageList />
      <MessageInput />
    </div>
  );
};

export default ChatWindow;
