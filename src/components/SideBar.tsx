import React from 'react';
import Link from 'next/link';

const SideBar: React.FC = () => {
  return (
    <div className="bg-gray-800 text-white w-64 min-h-screen p-4">
      {/* Add Channel or DM Links Here */}
      <Link href="/channel/general">General</Link>
      {/* More channels */}
    </div>
  );
};

export default SideBar;
