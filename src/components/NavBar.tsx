import React from 'react';
import Link from 'next/link';

const NavBar: React.FC = () => {
  return (
    <nav className="bg-blue-500 text-white p-4 flex">
      <Link href="/" className="mr-4">
        Home
      </Link>
      <Link href="/login" className="mr-4">
        Login
      </Link>
      <Link href="/signup">
        Signup
      </Link>
    </nav>
  );
};

export default NavBar;
