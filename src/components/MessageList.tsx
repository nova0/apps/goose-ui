import React from 'react';

const MessageList: React.FC = () => {
  return (
    <ul className="message-list">
      {/* List of messages will be rendered here */}
    </ul>
  );
};

export default MessageList;
