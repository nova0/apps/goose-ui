const Header: React.FC = () => {
  return (
    <header className="bg-gray-800 text-white p-4">
      <h1 className="text-xl">Goose UI</h1>
    </header>
  );
};

export default Header;
